package restify

import (
	"encoding/json"
	"net/http/httptest"
	"strings"
	"testing"

	"github.com/creichlin/collopi"
	"github.com/creichlin/restify/declaration"
)

type tc struct {
	method   string
	path     []string
	status   int
	response interface{}
}

var tests = []tc{
	{
		method:   "GET",
		path:     []string{},
		status:   200,
		response: "GET /",
	},
	{
		method:   "GET",
		path:     []string{"foo"},
		status:   200,
		response: "GET /foo",
	},
}

func TestX(t *testing.T) {
	for _, test := range tests {
		t.Run(test.method+" "+strings.Join(test.path, "/"), func(t *testing.T) {
			runCase(test, t)
		})
	}
}

func runCase(test tc, t *testing.T) {
	h, _, err := declaration.ReadDeclaration("testcases/a")
	if err != nil {
		t.Error(err)
	}

	server := httptest.NewServer(h)
	defer server.Close()

	c := collopi.NewClient(server.URL)

	response := interface{}(nil)
	status, err := c.Method(test.method).Path(test.path...).Target(&response).Do()

	if err != nil {
		t.Errorf("request failed with error: %v", err)
		return
	}
	if status != test.status {
		t.Errorf("status expected to be %v but was %v", test.status, status)
		return
	}

	rej := asJSON(response, t)
	rejExpected := asJSON(test.response, t)

	if rej != rejExpected {
		t.Errorf("returned wrong body\nexpected: %v\nactual: %v", rejExpected, rej)
	}
}

func asJSON(i interface{}, t *testing.T) string {
	d, err := json.MarshalIndent(i, "", "  ")
	if err != nil {
		t.Errorf("failed to marshall json, %v", err)
		return ""
	}
	return string(d)
}
