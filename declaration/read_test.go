package declaration

import "testing"

func TestRead(t *testing.T) {
	_, _, err := ReadDeclaration("../testcases/a")
	if err != nil {
		t.Error(err)
	}
}