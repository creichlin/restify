package declaration

import (
	"io/ioutil"
	"net/http"

	"github.com/creichlin/rip"
	"gopkg.in/yaml.v2"
)

func makeHandlerFor(dec string) (*handler, error) {
	d, err := ioutil.ReadFile(dec)
	if err != nil {
		return nil, err
	}
	hd := &handler{}
	err = yaml.Unmarshal(d, hd)

	if err != nil {
		return nil, err
	}

	return hd, nil
}

type handler struct {
	Text *string `json:"text"`
}

func (h *handler) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	if h.Text != nil {
		h.ServeText(w, r)
	}
}

func (h *handler) ServeText(w http.ResponseWriter, r *http.Request) {
	resp := rip.Response(r)
	resp.StatusCode = http.StatusOK
	resp.Data = h.Text
}
