package declaration

import (
	"io/ioutil"
	"net/http"
	"os"
	"path/filepath"
	"strings"

	"github.com/creichlin/rip"
)

func ReadDeclaration(root string) (http.Handler, http.Handler, error) {
	r := rip.NewRIP()
	route := r.AsRoute()

	err := addDeclaration(root, route)

	if err != nil {
		return nil, nil, err
	}

	h, err := r.RootHandler()
	return h, rip.DocHandler(r), err
}

func addDeclaration(path string, r *rip.Route) error {
	files, err := ioutil.ReadDir(path)
	if err != nil {
		return err
	}

	for _, file := range files {
		if file.IsDir() {
			addFolder(file, path, r)
		} else if file.Name() == "get.yaml" {
			h, err := makeHandlerFor(filepath.Join(path, file.Name()))
			if err != nil {
				return err
			}
			r.GET().Handler(h, "dummy")
		}

	}
	return nil
}

func addFolder(file os.FileInfo, path string, r *rip.Route) {
	if strings.HasPrefix(file.Name(), ":") {
		addDeclaration(
			filepath.Join(path, file.Name()),
			r.Var(file.Name()[1:], "dummy"),
		)
	} else {
		addDeclaration(
			filepath.Join(path, file.Name()),
			r.Path(file.Name()),
		)
	}
}
